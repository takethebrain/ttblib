package nl.kresict.games.ttb;

import java.util.Date;
import java.util.Scanner;
import nl.kresict.games.ttb.ai.AIPlayer;
import nl.kresict.games.ttb.domain.GameType;
import nl.kresict.games.ttb.domain.IPlayer;
import nl.kresict.games.ttb.domain.Move;
import nl.kresict.games.ttb.domain.TtbField;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.domain.HumanPlayer;
import nl.kresict.games.ttb.domain.Piece;
import nl.kresict.games.ttb.services.TtbBoardService;
import nl.kresict.games.ttb.services.TtbMoveDecisionHelper;
import nl.kresict.games.ttb.services.ex.EndOfGameException;
import nl.kresict.games.ttb.services.ex.HitException;
import nl.kresict.games.ttb.services.ex.IllegalMoveException;
import nl.kresict.games.ttb.services.ex.NinnyReplaceException;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class App 
{
    public static void main( String[] args ) {
        String gameType = "severe";
        if (gameType.equalsIgnoreCase("normal")) {
            runNormalGame();
        }
        else if (gameType.equalsIgnoreCase("severe")) {
            runAIGameSevere();
        }
        else {
            runAIGame();
        }
    }
    
    public static void runAIGameSevere() {
        IPlayer player1 = new AIPlayer("blue");
        IPlayer player2 = new AIPlayer("red");
        IPlayer turn = player1;
        TtbGameBoard board = TtbBoardService.initBoard(player1, player2);
        board.setType(GameType.HARD);
        System.out.println(board.toString());
        boolean gameEnd = false;
        boolean turnOver = false;
        int countTurns = 1;
        Date dateStart = new Date();
        while (!gameEnd) {
            while (!turnOver) {
                Move move;
                if (turn.getClass()==AIPlayer.class) {
                    AIPlayer p = (AIPlayer)turn;
                    move = p.getMove(board);
                    countTurns++;
                }
                else {
                    int[] moveFrom = makeMoveFrom(turn);
                    int[] moveTo = makeMoveTo(turn);
                    TtbField from = board.getField(moveFrom[0], moveFrom[1]);
                    TtbField to = board.getField(moveTo[0], moveTo[1]);
                    move = new Move(from, to);
                    
                }
                try {
                    TtbMoveDecisionHelper.isValidMove(board, move);
                    board.movePiece(move);
                    turnOver = true;
                    turn=board.getTurn();

                } catch (IllegalMoveException ex) {
                    System.err.println(ex.getMessage());
                } catch (HitException ex) {
                    if (ex.getMessage().startsWith("Ninny")) {
                        board.movePiece(move);
                        turnOver = true;
                        System.err.println(ex.getMessage());
                    } 
                } catch (NinnyReplaceException ex) {
                    board.replacePiece(move.getFrom(), Piece.NUMSCULL);
                    board.movePiece(move);
                    turnOver = true;
                    System.err.println(ex.getMessage());
                }
                catch (EndOfGameException ex) {
                    board.movePiece(move);
                    turnOver = true;
                    gameEnd = true;
                    System.err.println(ex.getMessage());
                }
            }
            turnOver=false;
            System.out.println(board.toString());
        }
        Date dateEnd = new Date();
        System.out.println("Turn count: " + countTurns);
        long difference = dateEnd.getTime() - dateStart.getTime(); 
        System.out.println("Time taken: " + difference + " milliseconds");
    }
    public static void runAIGame() {
        IPlayer player1 = new HumanPlayer("blue");
        IPlayer player2 = new AIPlayer("red");
        IPlayer turn = player1;
        TtbGameBoard board = TtbBoardService.initBoard(player1, player2);
        board.setType(GameType.HARD);
        System.out.println(board.toString());
        boolean gameEnd = false;
        boolean turnOver = false;
        while (!gameEnd) {
            while (!turnOver) {
                Move move;
                if (turn.getClass()==AIPlayer.class) {
                    AIPlayer p = (AIPlayer)turn;
                    move = p.getMove(board);
                }
                else {
                    int[] moveFrom = makeMoveFrom(turn);
                    int[] moveTo = makeMoveTo(turn);
                    TtbField from = board.getField(moveFrom[0], moveFrom[1]);
                    TtbField to = board.getField(moveTo[0], moveTo[1]);
                    move = new Move(from, to);
                    
                }
                try {
                    TtbMoveDecisionHelper.isValidMove(board, move);
                    board.movePiece(move);
                    turnOver = true;
                    turn=board.getTurn();

                } catch (IllegalMoveException ex) {
                    System.err.println(ex.getMessage());
                } catch (HitException ex) {
                    board.movePiece(move);
                    turnOver = true;
                    System.err.println(ex.getMessage());
                } catch (NinnyReplaceException ex) {
                    board.replacePiece(move.getFrom(), Piece.NUMSCULL);
                    board.movePiece(move);
                    turnOver = true;
                    System.err.println(ex.getMessage());
                }
                catch (EndOfGameException ex) {
                    board.movePiece(move);
                    gameEnd = true;
                    System.err.println(ex.getMessage());
                }
            }
            turnOver=false;
            System.out.println(board.toString());
        }
    }
    public static void runNormalGame() {
        HumanPlayer player1 = new HumanPlayer("blue");
        HumanPlayer player2 = new HumanPlayer("red");
        IPlayer turn = player1;
        TtbGameBoard board = TtbBoardService.initBoard(player1, player2);
        System.out.println(board.toString());
        boolean gameEnd = false;
        boolean turnOver = false;
        while (!gameEnd) {
            while (!turnOver) {
                int[] moveFrom = makeMoveFrom(turn);
                int[] moveTo = makeMoveTo(turn);
                TtbField from = board.getField(moveFrom[0], moveFrom[1]);
                TtbField to = board.getField(moveTo[0], moveTo[1]);
                try {
                    TtbMoveDecisionHelper.isValidMove(board, from, to);
                    board.movePiece(from, to);
                    turnOver = true;
                    turn=board.getTurn();

                } catch (IllegalMoveException ex) {
                    System.err.println(ex.getMessage());
                } catch (HitException ex) {
                    board.movePiece(from, to);
                    System.err.println(ex.getMessage());
                } catch (NinnyReplaceException ex) {
                    board.replacePiece(from, Piece.NUMSCULL);
                    board.movePiece(from, to);
                    System.err.println(ex.getMessage());
                } catch (EndOfGameException ex) {
                    board.movePiece(from, to);
                    gameEnd = true;
                    System.err.println(ex.getMessage());
                }
            }
            turnOver=false;
            System.out.println(board.toString());
        }
    }
    public static int[] makeMoveFrom(IPlayer player) {
        System.out.println("Make move from player " + player.getColor());
        return makeMove();
    }
    
    public static int[] makeMoveTo(IPlayer player) {
        return makeMove();
    }
    
    public static int[] makeMove() {
        Scanner in = new Scanner(System.in);
        int x;
        int y;
        // Reads a single line from the console 
        // and stores into name variable
        x = in.nextInt();

        // Reads a integer from the console
        // and stores into age variable
        y = in.nextInt();
        //in.close(); 
        int[] result = {x,y};
        return result;
    }
}
