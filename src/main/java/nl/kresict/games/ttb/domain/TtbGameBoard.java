package nl.kresict.games.ttb.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbGameBoard {
    
    //some static fields
    public static final int idxColumns=6;
    public static final int idxRows=7;
    
    //the board with fields
    private TtbField[][] ttbFields = new TtbField[idxRows+1][idxColumns+1];
    
    //the players
    private IPlayer player1;
    private IPlayer player2;
    private IPlayer turn;
    
    //game type
    private GameType type;

    public IPlayer getPlayer1() {
        return player1;
    }

    public IPlayer getPlayer2() {
        return player2;
    }

    public IPlayer getTurn() {
        return turn;
    }

    public void setTurn() {
        if (this.turn.equals(this.player1)) {
            this.turn=player2;
        }
        else {
            this.turn=player1;
        }
    }
    
    public List<TtbField> getFieldsOccupiedBy(IPlayer player) {
        List<TtbField> result = new ArrayList<TtbField>();
        for (TtbField[] ttbFields1 : ttbFields) {
            for (TtbField ttbField : ttbFields1) {
                if (ttbField.getOccupiedBy()!=null && ttbField.getOccupiedBy().getOwner().equals(player)) {
                    result.add(ttbField);
                }
            }
        }
        return result;
    }
    
    public List<TtbField> getDirectNeigbours(TtbField field) {
        List<TtbField> result = new ArrayList<TtbField>();
        int x=field.getX();
        int y=field.getY();
        if (x<=1) {
            for (int i = x-1; i <= x+1; i++) {
                if (i >= 0) {
                    for (int j = y-1; j <= y+1; j++) {
                        if (j >= 0 && j <= 6) {
                            if (i!=x || j!=y) {
                                TtbField neigbour = this.getField(i, j);
                                result.add(neigbour);
                            }
                        }
                    }
                }
            }
        }
        else {
            for (int i = x+1; i >= x-1; i--) {
                if (i <= 7) {
                    for (int j = y-1; j <= y+1; j++) {
                        if (j >= 0 && j <= 6) {
                            if (i!=x || j!=y) {
                                TtbField neigbour = this.getField(i, j);
                                result.add(neigbour);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }
    
    public TtbGameBoard(IPlayer player1, IPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.turn=player1;
    }

    public GameType getType() {
        return type;
    }

    public void setType(GameType type) {
        this.type = type;
    }
    
    public void addField(TtbField field) {
        if (field!=null) {
            int x=field.getX();
            int y=field.getY();
            //if (x>=0 && x<=idxColumns && y>=0 && y<=idxRows) {
                ttbFields[x][y] = field;
            //}
        }
    }
    
    public TtbField getField(int x, int y) {
        if (x>=0 && x<=idxRows && y>=0 && y<=idxColumns) {
            return ttbFields[x][y];
        }
        else {
            return null;
        }
    }

    public void movePiece(Move move) {
        movePiece(move.getFrom(), move.getTo());
    }
    
    public void replacePiece(TtbField field, Piece piece) {
        int x = field.getX();
        int y = field.getY();
        TtbPiece oldPiece = ttbFields[x][y].getOccupiedBy();
        oldPiece.setPiece(piece);
        ttbFields[x][y].setOccupiedBy(oldPiece);
    }
    
    public void movePiece(TtbField from, TtbField to) {
        int fromX=from.getX();
        int fromY=from.getY();
        int toX=to.getX();
        int toY=to.getY();
        TtbPiece piece = ttbFields[fromX][fromY].getOccupiedBy();
        ttbFields[fromX][fromY].setOccupiedBy(null);
        ttbFields[toX][toY].setOccupiedBy(piece);
        this.setTurn();
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (TtbField[] ttbFields1 : ttbFields) {
            for (TtbField ttbField : ttbFields1) {
                if (ttbField!=null) {
                    sb.append(ttbField.toString());
                }
                else {
                    sb.append("null");
                }
            }
            sb.append(System.lineSeparator());
        }
        return sb.toString();
    }
    
    
}
