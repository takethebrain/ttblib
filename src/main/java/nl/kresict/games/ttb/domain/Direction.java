package nl.kresict.games.ttb.domain;

/**
 * Defines all possible directions of a field
 * @author Sander IJpma - kres-ict
 */
public enum Direction {
    
    NORTH,
    SOUTH,
    WEST,
    EAST,
    NORTH_EAST,
    NORTH_WEST,
    SOUTH_EAST,
    SOUTH_WEST;
        
}
