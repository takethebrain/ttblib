package nl.kresict.games.ttb.domain;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public enum Piece {
    
    NUMSCULL("S"),
    BRAIN("B"),
    NINNY("N");
    
    private final String shortName;
    
    Piece(String shortName) {
        this.shortName=shortName;
    }
    
    public String getShortName() {
        return this.shortName;
    }

    @Override
    public String toString() {
        return super.toString();
    }
    

}
