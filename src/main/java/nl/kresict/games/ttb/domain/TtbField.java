package nl.kresict.games.ttb.domain;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbField {
 
    //x and y coordinates
    private int x;
    private int y;
    
    //posible directions
    private List<Direction> fieldDirectionList = new ArrayList<Direction>();
    
    //field possibly occupied by
    private TtbPiece occupiedBy;
    
    public TtbField(int x, int y) {
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public List<Direction> getFieldDirectionList() {
        return fieldDirectionList;
    }

    public void setFieldDirectionList(List<Direction> fieldDirectionList) {
        this.fieldDirectionList = fieldDirectionList;
    }

    public TtbPiece getOccupiedBy() {
        return occupiedBy;
    }

    public void setOccupiedBy(TtbPiece occupiedBy) {
        this.occupiedBy = occupiedBy;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{").append(this.x);
        sb.append(",").append(this.y);
        sb.append("}:");
        if (this.occupiedBy!=null) {
            sb.append(this.occupiedBy.getOwner().getColor().charAt(0));
            sb.append(this.occupiedBy.getPiece().getShortName());
        }
        else {
            sb.append("  ");
        }
        return sb.toString();
    }
    
    public String toStringCoordinates() {
        StringBuilder sb = new StringBuilder();
        sb.append("{").append(this.x+1);
        sb.append(",").append(this.y+1);
        sb.append("}");
        return sb.toString();
    }
    
    public String toStringZeroIndexedCoordinates() {
        StringBuilder sb = new StringBuilder();
        sb.append("{").append(this.x);
        sb.append(",").append(this.y);
        sb.append("}");
        return sb.toString();
    }
    
    public String toStringChessCoordinates() {
        StringBuilder sb = new StringBuilder();
        int xInt = ((TtbGameBoard.idxRows-1) - this.x) + 1  ; //6=1; 5=2; 4=3; 3=4; 2=5; 1=6
        char[] abc = {'a', 'b', 'c', 'd', 'e', 'f', 'g'};
        char yChar = abc[this.y];
        sb.append(yChar);
        sb.append(xInt+1);
        return sb.toString();
    }
    
    public String getFieldDirections() {
        StringBuilder sb = new StringBuilder();
        for (Direction fieldDirection : fieldDirectionList) {
            sb.append(fieldDirection.toString().charAt(0));
        }
        return sb.toString();
    }
    
    public boolean containsDirection(Direction direction) {
        if (direction!=null) {
            for (Direction fieldDirection : fieldDirectionList) {
                if (fieldDirection.equals(direction)) {
                    return true;
                }
            }
        }
        return false;
    }
}
