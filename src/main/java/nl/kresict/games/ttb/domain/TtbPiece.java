package nl.kresict.games.ttb.domain;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbPiece {
    
    private Piece piece;
    private IPlayer owner;

    public TtbPiece(Piece piece, IPlayer owner) {
        this.piece = piece;
        this.owner = owner;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public Piece getPiece() {
        return piece;
    }

    public IPlayer getOwner() {
        return owner;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.piece.getShortName());
        sb.append(":");
        sb.append(this.owner.getColor().charAt(0));
        return sb.toString();
    }

   
}
