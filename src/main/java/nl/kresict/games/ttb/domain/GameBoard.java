package nl.kresict.games.ttb.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * {Insert class description here}
 *
 * @author Sander IJpma | Krocket
  */
public class GameBoard {
    
    //Max row and column count
    protected int columnCount;
    protected int rowCount;
    
    //Array of fields
    protected List<List<Field>> fieldSet;

    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public List<List<Field>> getFieldSet() {
        if (fieldSet == null) {
            fieldSet = new ArrayList<List<Field>>();
        }
        return fieldSet;
    }

    public void setFieldSet(List<List<Field>> fieldSet) {
        this.fieldSet = fieldSet;
    }
    
}
