package nl.kresict.games.ttb.domain;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class HumanPlayer implements IPlayer{
    
    private String color;
    
    public HumanPlayer(String color) {
        this.color=color;
    }

    public String getColor() {
        return color;
    }
    
}
