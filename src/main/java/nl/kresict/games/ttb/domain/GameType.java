package nl.kresict.games.ttb.domain;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public enum GameType {
    
    EASY("Easy"),
    HARD("Hard"),
    HARDEST("Hardest");
    
    private String type;
    
    GameType(String type) {
        this.type=type;
    }

    public String getType() {
        return type;
    }
    
}
