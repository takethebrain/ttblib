
package nl.kresict.games.ttb.domain;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public interface IPlayer {
    
    public String getColor();
    
}
