package nl.kresict.games.ttb.domain;

/**
 *
 * @author Sander
 */
public class BestMove extends Move implements Comparable<BestMove> {
    
    private int score;
    
    public BestMove(TtbField from, TtbField to) {
        super(from, to);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    
    public Move getMove() {
        return new Move(this.getFrom(), this.getTo());
    }

    public int compareTo(BestMove o) {
        return o.getScore()-this.score;
    }
    
}
