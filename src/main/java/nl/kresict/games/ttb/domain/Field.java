package nl.kresict.games.ttb.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines a field on the game board
 *
 * @author Sander IJpma | Krocket
  */
public class Field {

    //Coordinates
    protected int x;
    protected int y;
    
    //Directions of field
    protected List<Direction> fieldDirectionList;
    
    //Occupied by
    protected TtbPiece occupiedBy;
    
    public Field(int x, int y) {
        this.x=x;
        this.y=y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public List<Direction> getFieldDirectionList() {
        if (fieldDirectionList == null) {
            fieldDirectionList = new ArrayList<Direction>();
        }
        return fieldDirectionList;
    }

    public void setFieldDirectionList(List<Direction> fieldDirectionList) {
        this.fieldDirectionList = fieldDirectionList;
    }
    
    public TtbPiece getOccupiedBy() {
        return occupiedBy;
    }

    public void setOccupiedBy(TtbPiece occupiedBy) {
        this.occupiedBy = occupiedBy;
    }
            
}
