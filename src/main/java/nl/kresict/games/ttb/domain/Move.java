package nl.kresict.games.ttb.domain;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class Move {
    
    private TtbField from;
    private TtbField to;

    public Move(TtbField from, TtbField to) {
        this.from = from;
        this.to = to;
    }

    public TtbField getFrom() {
        return from;
    }

    public TtbField getTo() {
        return to;
    }
    
    public boolean isValidMove() {
       if (from.containsDirection(getDirection(this.from, this.to))) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public Direction getDirection(TtbField from, TtbField to) {
        int fromX = from.getX();
        int fromY = from.getY();
        int toX = to.getX();
        int toY = to.getY();
        if (fromX>toX && fromY==toY) {
            return Direction.NORTH;
        }
        else if (fromX>toX && fromY>toY) {
            return Direction.NORTH_WEST;
        }
        else if (fromX>toX && fromY<toY) {
            return Direction.NORTH_EAST;
        }
        else if (fromX==toX && fromY>toY) {
            return Direction.WEST;
        }
        else if (fromX==toX && fromY<toY) {
            return Direction.EAST;
        }
        else if (fromX<toX && fromY==toY) {
            return Direction.SOUTH;
        }
        else if (fromX<toX && fromY>toY) {
            return Direction.SOUTH_WEST;
        }
        else if (fromX<toX && fromY<toY) {
            return Direction.SOUTH_EAST;
        }
        else {
            return null;
        }
    }
   
}
