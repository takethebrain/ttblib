package nl.kresict.games.ttb.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import nl.kresict.games.ttb.domain.BestMove;
import nl.kresict.games.ttb.domain.IPlayer;
import nl.kresict.games.ttb.domain.Move;
import nl.kresict.games.ttb.domain.TtbField;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.services.TtbMoveDecisionHelper;
import nl.kresict.games.ttb.services.ex.EndOfGameException;
import nl.kresict.games.ttb.services.ex.HitException;
import nl.kresict.games.ttb.services.ex.IllegalMoveException;
import nl.kresict.games.ttb.services.ex.NinnyReplaceException;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class AIService {
    
    IPlayer aIPlayer;
    TtbGameBoard board;
    
    public AIService (TtbGameBoard board) {
        this.aIPlayer = board.getTurn();
        this.board = board;
    }

    public Move getNextBestMove() {
        List<BestMove> moveAbleFields = getBestMoveableFields();
        Move move = moveAbleFields.get(0).getMove();
        return move;
    }
    
    public Move getNextMove() {
        List<Move> moveAbleFields = getMoveableFields();
        //randomly select one
        Random generator = new Random();
        int idx = generator.nextInt(moveAbleFields.size());
        return moveAbleFields.get(idx);
    }
    
    private List<Move> getMoveableFields() {
        List<Move> moveAbleFields = new ArrayList<Move>();
        List<TtbField> fields = board.getFieldsOccupiedBy(aIPlayer);
        for (TtbField from : fields) {
            List<TtbField> neigbours = board.getDirectNeigbours(from);
            for (TtbField to : neigbours) {
                Move newPossibleMove = new Move(from, to);
                try {
                    boolean result = TtbMoveDecisionHelper.isValidMove(board, newPossibleMove);
                    if (result) {
                        moveAbleFields.add(newPossibleMove);
                    }
                } catch (IllegalMoveException ex) {
                    //no action
                } catch (HitException ex) {
                    moveAbleFields.add(newPossibleMove);
                } catch (NinnyReplaceException ex) {
                    moveAbleFields.add(newPossibleMove);
                } catch (EndOfGameException ex) {
                    moveAbleFields.add(newPossibleMove);
                }
            }
        }
        return moveAbleFields;
    }
    
    private List<BestMove> getBestMoveableFields() {
        List<BestMove> moveAbleFields = new ArrayList<BestMove>();
        List<TtbField> fields = board.getFieldsOccupiedBy(aIPlayer);
        for (TtbField from : fields) {
            List<TtbField> neigbours = board.getDirectNeigbours(from);
            for (TtbField to : neigbours) {
                BestMove newPossibleMove = new BestMove(from, to);
                try {
                    boolean result = TtbMoveDecisionHelper.isValidMove(board, newPossibleMove);
                    if (result) {
                        if (isForwardMove(newPossibleMove)) {
                            newPossibleMove.setScore(10);
                        }
                        else {
                            newPossibleMove.setScore(5);
                        }
                        moveAbleFields.add(newPossibleMove);
                    }
                } catch (IllegalMoveException ex) {
                   //do nothing
                } catch (HitException ex) {
                    newPossibleMove.setScore(50);
                    moveAbleFields.add(newPossibleMove);
                } catch (NinnyReplaceException ex) {
                    newPossibleMove.setScore(70);
                    moveAbleFields.add(newPossibleMove);
                } catch (EndOfGameException ex) {
                    newPossibleMove.setScore(100);
                    moveAbleFields.add(newPossibleMove);
                }
            }
        }
        Collections.sort(moveAbleFields);
        return moveAbleFields;
    }

    private boolean isForwardMove(BestMove newPossibleMove) {
        String color = newPossibleMove.getFrom().getOccupiedBy().getOwner().getColor();
        if (color.equalsIgnoreCase("red")) {
            if (newPossibleMove.getFrom().getX() < newPossibleMove.getTo().getX()) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if (newPossibleMove.getFrom().getX() > newPossibleMove.getTo().getX()) {
                return true;
            }
            else {
                return false;
            }
        }
    }
}
