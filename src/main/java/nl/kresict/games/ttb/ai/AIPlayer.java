package nl.kresict.games.ttb.ai;

import java.util.ArrayList;
import java.util.List;
import nl.kresict.games.ttb.domain.GameType;
import nl.kresict.games.ttb.domain.IPlayer;
import nl.kresict.games.ttb.domain.Move;
import nl.kresict.games.ttb.domain.Piece;
import nl.kresict.games.ttb.domain.TtbGameBoard;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class AIPlayer implements IPlayer{

    private String color;
    public List<Piece> lostPieces;
    
    public AIPlayer(String color) {
        this.color=color;
    } 

    public List<Piece> getLostPieces() {
        if (lostPieces!=null) {
            return lostPieces;
        }
        else {
            return new ArrayList<Piece>();
        }
    }
    
    public String getColor() {
        return this.color;
    }
    
    public Move getMove(TtbGameBoard board) {
        AIService aiService = new AIService(board);
        if (board.getType().equals(GameType.HARD)) {
            return aiService.getNextBestMove();
        }
        else {
            return aiService.getNextMove();
        }
    }
       
}
