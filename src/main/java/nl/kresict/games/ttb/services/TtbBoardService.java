package nl.kresict.games.ttb.services;

import nl.kresict.games.ttb.domain.GameType;
import nl.kresict.games.ttb.domain.IPlayer;
import nl.kresict.games.ttb.domain.TtbField;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.domain.HumanPlayer;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbBoardService {
    
    public static TtbGameBoard initBoard() {
        return initBoard(new HumanPlayer("blue"), new HumanPlayer("red"));
    }
    
    public static TtbGameBoard initBoard(IPlayer player1, IPlayer player2) {
        TtbFieldService fieldService = new TtbFieldService(player1, player2);
        
        TtbGameBoard board = new TtbGameBoard(player1, player2);
        for (int i = 0; i <= TtbGameBoard.idxRows; i++) {
            for (int j = 0; j <= TtbGameBoard.idxColumns; j++) {
                //TtbField field = fieldService.getField(i, j);
                //board.addField(field);
            }
        }
        return board;
    }
    
    public static TtbGameBoard initBoard(IPlayer player1, IPlayer player2, GameType type) {
        TtbFieldService fieldService = new TtbFieldService(player1, player2);
        
        TtbGameBoard board = new TtbGameBoard(player1, player2);
        for (int i = 0; i <= TtbGameBoard.idxRows; i++) {
            for (int j = 0; j <= TtbGameBoard.idxColumns; j++) {
                //TtbField field = fieldService.getField(i, j);
                //board.addField(field);
            }
        }
        board.setType(type);
        return board;
    }
    
}
