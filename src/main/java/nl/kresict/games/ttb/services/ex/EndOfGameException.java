package nl.kresict.games.ttb.services.ex;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class EndOfGameException extends Exception{
    
    public EndOfGameException(String message) {
        super(message);
    }
}
