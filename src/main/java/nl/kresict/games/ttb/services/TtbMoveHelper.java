package nl.kresict.games.ttb.services;

import java.util.ArrayList;
import java.util.List;
import nl.kresict.games.ttb.domain.Move;
import nl.kresict.games.ttb.domain.TtbField;
import nl.kresict.games.ttb.domain.TtbGameBoard;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbMoveHelper {
    
   
    public List<Move> getMoves(TtbGameBoard board, TtbField from, TtbField to) {
        List<Move> moves = new ArrayList<Move>();
            //naar west: y_van > y_naar; x gelijk; {0,2} -> {0,1}
            if (from.getY() > to.getY() && from.getX() == to.getX()) {
                for (int i = from.getY(); i > to.getY(); i--) {
                    TtbField newTo = board.getField(from.getX(), i-1);
                    moves.add(new Move(from, newTo));
                    from = newTo;
                }
            }
            //naar oost: y_van < y_naar; x gelijk; {0,1} -> {0,2}
            else if (from.getY() < to.getY() && from.getX() == to.getX()) {
                for (int i = from.getY(); i < to.getY(); i++) {
                    TtbField newTo = board.getField(from.getX(), i+1);
                    moves.add(new Move(from, newTo));
                    from = newTo;
                }
            }
            //naar zuid: x_van < x_naar: y gelijk; {0,0} -> {1,0}
            else if (from.getX() < to.getX() && from.getY() == to.getY()) {
                for (int i = from.getX(); i < to.getX(); i++) {
                    TtbField newTo = board.getField(i+1, from.getY());
                    moves.add(new Move(from, newTo));
                    from = newTo;
                }
            }
            //naar noord: x_van > x_naar: y gelijk; {1,0} -> {0,0}
            else if (from.getX() > to.getX() && from.getY() == to.getY()) {
                for (int i = from.getX(); i > to.getX(); i--) {
                    TtbField newTo = board.getField(i-1, from.getY());
                    moves.add(new Move(from, newTo));
                    from = newTo;
                }
            }
            //naar zuid_west: x_van < x_naar: y_van > y_naar; {0,1} -> {1,0}
            else if (from.getX() < to.getX() && from.getY() > to.getY()) {
                int i = from.getX();
                int j = from.getY();
                while (i<to.getX() && j>to.getY()) {
                    TtbField newTo = board.getField(i+1, j-1);
                        moves.add(new Move(from, newTo));
                        from = newTo;
                        i++;
                        j--;
                }
            }
            //naar zuid_oost: x_van < x_naar; y_van < y_naar; {0,0} -> {1,1}
            else if (from.getX() < to.getX() && from.getY() < to.getY()) {
                int i = from.getX();
                int j = from.getY();
                while (i<to.getX() && j<to.getY()) {
                    TtbField newTo = board.getField(i+1, j+1);
                        moves.add(new Move(from, newTo));
                        from = newTo;
                        i++;
                        j++;
                }
            }
            //naar noord_west: x_van > x_naar; y_van > y_naar; {1,1} -> {0,0}
            else if (from.getX() > to.getX() && from.getY() > to.getY()) {
                int i = from.getX();
                int j = from.getY();
                while (i>to.getX() && j>to.getY()) {
                    TtbField newTo = board.getField(i-1, j-1);
                        moves.add(new Move(from, newTo));
                        from = newTo;
                        i--;
                        j--;
                }
            }
            //naar noord_oost: x_van > x_naar; y_van < y_naar; {1,0} -> {0,1}
            else if (from.getX() > to.getX() && from.getY() < to.getY()) {
                int i = from.getX();
                int j = from.getY();
                while (i>to.getX() && j<to.getY()) {
                    TtbField newTo = board.getField(i-1, j+1);
                        moves.add(new Move(from, newTo));
                        from = newTo;
                        i--;
                        j++;
                }
            }
        return moves;
    }
    
    public boolean isOneDirectionMove(TtbField from, TtbField to) {
        if (from!=null && to!=null) {
            int fromMinusToX = from.getX() - to.getX();
            int fromMinusToY = from.getY() - to.getY();
            if (fromMinusToX<0){
                fromMinusToX = -fromMinusToX;
            }
            if (fromMinusToY<0){
                fromMinusToY = -fromMinusToY;
            }
            //{1,0} -> {0,1} = 1, -1
            //{1,1} -> {0,0} = 1, 1
            //{0,0} -> {1,1} = -1, -1
            //{0,1} -> {1,0} = -1, 1
            if (fromMinusToX == fromMinusToY) {
                return true;
            }
            //{1,0} -> {0,0} = 1, 0
            //{0,0} -> {1,0} = -1, 0
            //{0,1} -> {0,2} = 0, -1
            //{0,2} -> {0,1} = 0, 1
            else if (fromMinusToX == 0 && fromMinusToY > 0) {
                return true;
            }
            else if (fromMinusToY == 0 && fromMinusToX > 0) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    
}
