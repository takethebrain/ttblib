package nl.kresict.games.ttb.services.ex;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class NinnyReplaceException extends Exception{
    
    public NinnyReplaceException(String message) {
        super(message);
    }
}
