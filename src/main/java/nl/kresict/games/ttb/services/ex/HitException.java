package nl.kresict.games.ttb.services.ex;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class HitException extends Exception{
    
    public HitException(String message) {
        super(message);
    }
}
