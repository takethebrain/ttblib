package nl.kresict.games.ttb.services;

import java.util.List;
import nl.kresict.games.ttb.domain.Move;
import nl.kresict.games.ttb.domain.Piece;
import nl.kresict.games.ttb.domain.TtbField;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.domain.TtbPiece;
import nl.kresict.games.ttb.services.ex.EndOfGameException;
import nl.kresict.games.ttb.services.ex.HitException;
import nl.kresict.games.ttb.services.ex.IllegalMoveException;
import nl.kresict.games.ttb.services.ex.NinnyReplaceException;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbMoveDecisionHelper {

    public static boolean isValidMove(TtbGameBoard board, Move move) throws IllegalMoveException, EndOfGameException, HitException, NinnyReplaceException {
        return isValidMove(board, move.getFrom(), move.getTo());
    }

    public static boolean isValidMove(TtbGameBoard board, TtbField from, TtbField to) throws IllegalMoveException, EndOfGameException, HitException, NinnyReplaceException {
        boolean result = true;
        TtbMoveHelper helper = new TtbMoveHelper();
        List<Move> moves;
        boolean isMove = !from.equals(to);
        boolean isTurn = from.getOccupiedBy().getOwner().equals(board.getTurn());
        boolean isOneDirection = helper.isOneDirectionMove(from, to);
        if (isMove && isTurn && isOneDirection) {
            moves = helper.getMoves(board, from, to);
            if (moves.get(0).isValidMove()) {
                int size = moves.size();
                if (from.getOccupiedBy().getPiece().equals(Piece.BRAIN) && size > 1) {
                    throw new IllegalMoveException("Brain cannot move more then one place!");
                } else if (from.getOccupiedBy().getPiece().equals(Piece.NINNY) && size > 1) {
                    throw new IllegalMoveException("Ninny cannot move more then one place!");
                } else {
                    //no other 'move more then' rules
                }
                
                if (to.getX() == TtbGameBoard.idxRows && from.getOccupiedBy().getOwner().getColor().equalsIgnoreCase("red") && from.getOccupiedBy().getPiece().equals(Piece.NINNY) && (to.getOccupiedBy()==null || !to.getOccupiedBy().getPiece().equals(Piece.BRAIN))) {
                    throw new NinnyReplaceException("Red ninny overtakes numskull!");
                } else if (to.getX() == 0 && from.getOccupiedBy().getOwner().getColor().equalsIgnoreCase("blue") && from.getOccupiedBy().getPiece().equals(Piece.NINNY) && (to.getOccupiedBy()==null || !to.getOccupiedBy().getPiece().equals(Piece.BRAIN))) {
                    throw new NinnyReplaceException("Blue ninny overtakes numskull!");
                } else {
                    //no more replace piece rules
                }
                
                TtbPiece lastOwner = to.getOccupiedBy();
                if (lastOwner != null && lastOwner.getOwner().equals(board.getTurn())) {
                    throw new IllegalMoveException("Can not hit your own piece!");
                } else if (lastOwner != null && lastOwner.getPiece().equals(Piece.BRAIN)) {
                    throw new EndOfGameException("Brain Captured! " + board.getTurn().getColor() + " wins!");
                } else if (lastOwner != null && to.getOccupiedBy().getPiece().equals(Piece.NINNY)) {
                    throw new HitException("Ninny taken!");
                } else if (lastOwner != null && to.getOccupiedBy().getPiece().equals(Piece.NUMSCULL)) {
                    throw new HitException("Numskull taken!");
                } else {
                    //no other hit rules
                }

                int count = 0;
                for (Move move : moves) {
                    if (count > 0 && count < moves.size()) {
                        if (move.getFrom().getOccupiedBy() != null) {
                            throw new IllegalMoveException("Can not go over a piece!");
                        }
                    }
                    count++;
                    if (!from.getOccupiedBy().getPiece().equals(Piece.NUMSCULL) && !move.isValidMove()) {
                        throw new IllegalMoveException("Can not go this way from field: " + move.getFrom().toStringCoordinates());
                    } else {
                        //numscull can go any direction as designated from starting point
                    }
                }
            } else {
                throw new IllegalMoveException("Can not go this way from field: " + moves.get(0).getFrom().toStringCoordinates());
            }
        } else if (!isMove && isTurn && isOneDirection) {
            throw new IllegalMoveException("Not a move!");
        } else if (isMove && !isTurn && isOneDirection) {
            throw new IllegalMoveException("Not your turn!");
        } else if (isMove && isTurn && !isOneDirection) {
            throw new IllegalMoveException("Can only go in one direction!");
        } else if (!isMove && !isTurn && isOneDirection) {
            throw new IllegalMoveException("Not your turn and not a move!");
        } else if (!isMove && isTurn && !isOneDirection) { //should never occur
            throw new IllegalMoveException("Not a move and not in one direction!"); 
        } else if (isMove && !isTurn && !isOneDirection) {
            throw new IllegalMoveException("Not your turn and not in one direction!");
        } else { //should never occur
            throw new IllegalMoveException("Not a move, not your turn and not in one direction!"); 
        }
        return result;
    }
}
