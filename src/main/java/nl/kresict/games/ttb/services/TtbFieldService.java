package nl.kresict.games.ttb.services;

import nl.kresict.games.ttb.domain.Direction;
import nl.kresict.games.ttb.domain.Field;
import nl.kresict.games.ttb.domain.IPlayer;
import nl.kresict.games.ttb.domain.Piece;
import nl.kresict.games.ttb.domain.Field;
import nl.kresict.games.ttb.domain.TtbPiece;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbFieldService {
    
    private IPlayer player1;
    private IPlayer player2;
    
    public TtbFieldService(IPlayer player1, IPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }
    
    public Field getField(int x, int y) {
        Field field;
        //first row
        if (x==0 && y==0) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==0 && y==1) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NUMSCULL, player2));
            return field;
        }
        else if (x==0 && y==2) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NUMSCULL, player2));
            return field;
        }
        else if (x==0 && y==3) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.BRAIN, player2));
            return field;
        }
        else if (x==0 && y==4) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NUMSCULL, player2));
            return field;
        }
        else if (x==0 && y==5) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NUMSCULL, player2));
            return field;
        }
        else if (x==0 && y==6) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        //end of first row
        //second row
        else if (x==1 && y==0) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player2));
            return field;
        }
        else if (x==1 && y==1) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.NORTH_WEST);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player2));
            return field;
        }
        else if (x==1 && y==2) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player2));
            return field;
        }
        else if (x==1 && y==3) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player2));
            return field;
        }
        else if (x==1 && y==4) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player2));
            return field;
        }
        else if (x==1 && y==5) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player2));
            return field;
        }
        else if (x==1 && y==6) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player2));
            return field;
        }
        //end of second row
        //third row
        else if (x==2 && y==0) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==2 && y==1) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==2 && y==2) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH_EAST);
            field.getFieldDirectionList().add(Direction.NORTH_WEST);
            field.getFieldDirectionList().add(Direction.SOUTH_EAST);
            field.getFieldDirectionList().add(Direction.SOUTH_WEST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==2 && y==3) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==2 && y==4) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH_EAST);
            field.getFieldDirectionList().add(Direction.NORTH_WEST);
            field.getFieldDirectionList().add(Direction.SOUTH_EAST);
            field.getFieldDirectionList().add(Direction.SOUTH_WEST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==2 && y==5) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==2 && y==6) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.WEST);
            field.setOccupiedBy(null);
            return field;
        }
        //end of third row
        //fourth row
        else if (x==3 && y==0) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.NORTH_EAST);
            field.getFieldDirectionList().add(Direction.SOUTH_EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==3 && y==1) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==3 && y==2) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==3 && y==3) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.NORTH_EAST);
            field.getFieldDirectionList().add(Direction.NORTH_WEST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.SOUTH_EAST);
            field.getFieldDirectionList().add(Direction.SOUTH_WEST);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==3 && y==4) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==3 && y==5) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==3 && y==6) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        //end of fourth row
        //fifth row
        else if (x==4 && y==0) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==4 && y==1) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.WEST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==4 && y==2) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==4 && y==3) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.NORTH_EAST);
            field.getFieldDirectionList().add(Direction.NORTH_WEST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.SOUTH_EAST);
            field.getFieldDirectionList().add(Direction.SOUTH_WEST);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==4 && y==4) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==4 && y==5) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==4 && y==6) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.SOUTH_WEST);
            field.getFieldDirectionList().add(Direction.NORTH_WEST);
            field.setOccupiedBy(null);
            return field;
        }
        //end of fifth row
        //sixth row
        else if (x==5 && y==0) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==5 && y==1) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.WEST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==5 && y==2) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH_WEST);
            field.getFieldDirectionList().add(Direction.NORTH_EAST);
            field.getFieldDirectionList().add(Direction.SOUTH_WEST);
            field.getFieldDirectionList().add(Direction.SOUTH_EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==5 && y==3) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==5 && y==4) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH_WEST);
            field.getFieldDirectionList().add(Direction.NORTH_EAST);
            field.getFieldDirectionList().add(Direction.SOUTH_WEST);
            field.getFieldDirectionList().add(Direction.SOUTH_EAST);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==5 && y==5) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==5 && y==6) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(null);
            return field;
        }
        //end of sixth row
        //seventh row
        else if (x==6 && y==0) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player1));
            return field;
        }
        else if (x==6 && y==1) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player1));
            return field;
        }
        else if (x==6 && y==2) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player1));
            return field;
        }
        else if (x==6 && y==3) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player1));
            return field;
        }
        else if (x==6 && y==4) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player1));
            return field;
        }
        else if (x==6 && y==5) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH_EAST);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player1));
            return field;
        }
        else if (x==6 && y==6) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.SOUTH);
            field.setOccupiedBy(new TtbPiece(Piece.NINNY, player1));
            return field;
        }
        //end of seventh row
        //eighth row
        else if (x==7 && y==0) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.setOccupiedBy(null);
            return field;
        }
        else if (x==7 && y==1) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.WEST);
            field.setOccupiedBy(new TtbPiece(Piece.NUMSCULL, player1));
            return field;
        }
        else if (x==7 && y==2) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.setOccupiedBy(new TtbPiece(Piece.NUMSCULL, player1));
            return field;
        }
        else if (x==7 && y==3) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.setOccupiedBy(new TtbPiece(Piece.BRAIN, player1));
            return field;
        }
        else if (x==7 && y==4) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.EAST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.setOccupiedBy(new TtbPiece(Piece.NUMSCULL, player1));
            return field;
        }
        else if (x==7 && y==5) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.WEST);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.EAST);
            field.setOccupiedBy(new TtbPiece(Piece.NUMSCULL, player1));
            return field;
        }
        else if (x==7 && y==6) {
            field = new Field(x, y);
            field.getFieldDirectionList().add(Direction.NORTH);
            field.getFieldDirectionList().add(Direction.WEST);
            field.setOccupiedBy(null);
            return field;
        }
        //end of eigth row
        else {
            return null;
        }
    }
}
