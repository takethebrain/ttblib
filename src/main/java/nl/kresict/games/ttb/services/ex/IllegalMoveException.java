package nl.kresict.games.ttb.services.ex;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class IllegalMoveException extends Exception{
    
    public IllegalMoveException(String message) {
        super(message);
    }
}
