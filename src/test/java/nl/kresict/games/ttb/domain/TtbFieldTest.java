package nl.kresict.games.ttb.domain;

import java.util.List;
import junit.framework.TestCase;
import nl.kresict.games.ttb.services.TtbBoardService;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbFieldTest extends TestCase {
    
    public TtbFieldTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getFieldDirectionList method, of class TtbField.
     */
    public void testGetFieldDirectionList() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField instance = board.getField(0, 0);
        List result = instance.getFieldDirectionList();
        assertEquals(2, result.size());
    }

    /**
     * Test of getOccupiedBy method, of class TtbField.
     */
    public void testGetOccupiedBy() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(1, 0);
        TtbPiece result = field.getOccupiedBy();
        assertEquals(Piece.NINNY, result.getPiece());
    }
    
    /**
     * Test of getOccupiedBy method, of class TtbField.
     */
    public void testGetOccupiedByIsNull() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(0, 0);
        TtbPiece result = field.getOccupiedBy();
        assertNull(result);
    }

    /**
     * Test of toStringCoordinates method, of class TtbField.
     */
    public void testToStringCoordinates() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(0, 0);
        assertEquals("{1,1}", field.toStringCoordinates());
    }

    /**
     * Test of toStringZeroIndexedCoordinates method, of class TtbField.
     */
    public void testToStringZeroIndexedCoordinates() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(0, 0);
        assertEquals("{0,0}", field.toStringZeroIndexedCoordinates());
    }

    /**
     * Test of toStringChessCoordinates method, of class TtbField.
     */
    public void testToStringChessCoordinatesTopLeft() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(0, 0);
        assertEquals("a8", field.toStringChessCoordinates());
    }
    
    /**
     * Test of toStringChessCoordinates method, of class TtbField.
     */
    public void testToStringChessCoordinatesBottemLeft() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(7, 0);
        assertEquals("a1", field.toStringChessCoordinates());
    }
    
    /**
     * Test of toStringChessCoordinates method, of class TtbField.
     */
    public void testToStringChessCoordinatesBottemRight() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(7, 6);
        assertEquals("g1", field.toStringChessCoordinates());
    }
    
    /**
     * Test of toStringChessCoordinates method, of class TtbField.
     */
    public void testToStringChessCoordinatesTopRight() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(0, 6);
        assertEquals("g8", field.toStringChessCoordinates());
    }

   
}
