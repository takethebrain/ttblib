package nl.kresict.games.ttb.domain;

import java.util.List;
import junit.framework.TestCase;
import nl.kresict.games.ttb.services.TtbBoardService;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbGameBoardTest extends TestCase {
    
    public TtbGameBoardTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getFieldsOccupiedBy method, of class TtbGameBoard.
     */
    public void testGetFieldsOccupiedBy() {
        TtbGameBoard board = TtbBoardService.initBoard();
        IPlayer player = board.getPlayer1();
        List result = board.getFieldsOccupiedBy(player);
        assertEquals(12, result.size());
    }

    /**
     * Test of getDirectNeigbours method, of class TtbGameBoard.
     */
    public void testGetDirectNeigboursTopLeft() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(0, 0);
        List<TtbField> result = board.getDirectNeigbours(field);
        for (TtbField ttbField : result) {
            assertNotNull(ttbField);
        }
        assertEquals(3, result.size());
    }
    
    /**
     * Test of getDirectNeigbours method, of class TtbGameBoard.
     */
    public void testGetDirectNeigboursTopRight() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(0, 6);
        List<TtbField> result = board.getDirectNeigbours(field);
        for (TtbField ttbField : result) {
            assertNotNull(ttbField);
        }
        assertEquals(3, result.size());
    }
    
    /**
     * Test of getDirectNeigbours method, of class TtbGameBoard.
     */
    public void testGetDirectNeigboursTopMiddle() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(0, 3);
        List<TtbField> result = board.getDirectNeigbours(field);
        for (TtbField ttbField : result) {
            assertNotNull(ttbField);
        }
        assertEquals(5, result.size());
    }
    
    /**
     * Test of getDirectNeigbours method, of class TtbGameBoard.
     */
    public void testGetDirectNeigboursMiddle() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(1, 3);
        List<TtbField> result = board.getDirectNeigbours(field);
        for (TtbField ttbField : result) {
            assertNotNull(ttbField);
        }
        assertEquals(8, result.size());
    }
    
    /**
     * Test of getDirectNeigbours method, of class TtbGameBoard.
     */
    public void testGetDirectNeigboursSomewhere() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(6, 6);
        List<TtbField> result = board.getDirectNeigbours(field);
        for (TtbField ttbField : result) {
            assertNotNull(ttbField);
        }
        assertEquals(5, result.size());
    }
    
    /**
     * Test of getDirectNeigbours method, of class TtbGameBoard.
     */
    public void testGetDirectNeigboursBottomLeft() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(7, 0);
        List<TtbField> result = board.getDirectNeigbours(field);
        for (TtbField ttbField : result) {
            assertNotNull(ttbField);
        }
        assertEquals(3, result.size());
    }
    
    /**
     * Test of getDirectNeigbours method, of class TtbGameBoard.
     */
    public void testGetDirectNeigboursBottomRight() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField field = board.getField(7, 6);
        List<TtbField> result = board.getDirectNeigbours(field);
        for (TtbField ttbField : result) {
            assertNotNull(ttbField);
        }
        assertEquals(3, result.size());
    }


    /**
     * Test of getField method, of class TtbGameBoard.
     */
    public void testGetFieldNotNullTopLeftCorner() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField result = board.getField(0, 0);
        assertNotNull(result);
    }

    /**
     * Test of getField method, of class TtbGameBoard.
     */
    public void testGetFieldNotNullBottemRightCorner() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField result = board.getField(7, 6);
        assertNotNull(result);
    }
}
