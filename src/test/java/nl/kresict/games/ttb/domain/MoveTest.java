package nl.kresict.games.ttb.domain;

import junit.framework.TestCase;
import nl.kresict.games.ttb.services.TtbBoardService;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class MoveTest extends TestCase {
    
    public MoveTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    

    /**
     * Test of isValidMove method, of class Move.
     */
    public void testIsValidMove() {
        TtbGameBoard board = TtbBoardService.initBoard();
        TtbField fieldFrom = board.getField(0, 1);
        TtbField fieldTo = board.getField(0, 2);
        Move instance = new Move(fieldFrom, fieldTo);
        boolean expResult = true;
        boolean result = instance.isValidMove();
        assertEquals(expResult, result);
    }


}
