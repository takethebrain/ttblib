package nl.kresict.games.ttb.domain;

import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class PieceTest extends TestCase {
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testContainsPiece() {
        List<Piece> pieces = new ArrayList<Piece>();
        pieces.add(Piece.BRAIN);
        assertTrue(pieces.contains(Piece.BRAIN));
    }
    
    public void testRemovePiece() {
        List<Piece> pieces = new ArrayList<Piece>();
        pieces.add(Piece.BRAIN);
        pieces.remove(Piece.BRAIN);
        assertTrue(pieces.isEmpty());
        assertFalse(pieces.contains(Piece.BRAIN));
    }
    
}
