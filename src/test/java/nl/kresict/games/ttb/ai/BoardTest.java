/*
 * Copyright(c) 2015 Krocket VOF All Rights Reserved.
 * This software is the proprietary information of Krocket VOF
 * Created on May 21, 2015 by Sander using UTF-8 encoding
 */
package nl.kresict.games.ttb.ai;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import nl.kresict.games.ttb.domain.Field;
import nl.kresict.games.ttb.domain.GameBoard;
import nl.kresict.games.ttb.domain.HumanPlayer;
import nl.kresict.games.ttb.domain.IPlayer;
import nl.kresict.games.ttb.services.TtbFieldService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Sander
 */
public class BoardTest {
    
    public BoardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
     @Test
     public void hello() throws Exception {
         GameBoard board = initBoard(new HumanPlayer("blue"), new HumanPlayer("red"));
         ObjectMapper mapper = new ObjectMapper();
         String str = mapper.writeValueAsString(board);
         
         try {
             Writer writer = new BufferedWriter(
                new OutputStreamWriter(
                    new FileOutputStream("ttb-board.json"), "utf-8")); 
                writer.write(str);
                writer.close();
            }
         catch (Exception e) {
             System.err.println(e);
         }
     }
     
     public static GameBoard initBoard(IPlayer player1, IPlayer player2) {
        TtbFieldService fieldService = new TtbFieldService(player1, player2);
        
        GameBoard board = new GameBoard();
        board.setRowCount(7);
        board.setColumnCount(6);
        for (int i = 0; i <= board.getRowCount(); i++) {
            List<Field> row = new ArrayList<Field>();
            for (int j = 0; j <= board.getColumnCount(); j++) {
                Field field = fieldService.getField(i, j);
                row.add(field);
            }
            board.getFieldSet().add(row);
        }
        return board;
    }
}
