package nl.kresict.games.ttb.ai;

import junit.framework.TestCase;
import nl.kresict.games.ttb.domain.Move;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.services.TtbBoardService;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class AIServiceTest extends TestCase {
    
    public AIServiceTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getMove method, of class AIService.
     */
    public void testGetMove() {
        TtbGameBoard board = TtbBoardService.initBoard();
        AIService instance = new AIService(board);
        Move result = instance.getNextMove();
        assertNotNull(result.getFrom());
        assertNotNull(result.getTo());
    }
    
    /**
     * Test of getMove method, of class AIService.
     */
    public void testGetNextBestMove() {
        TtbGameBoard board = TtbBoardService.initBoard();
        AIService instance = new AIService(board);
        Move result = instance.getNextBestMove();
        assertNotNull(result.getFrom());
        assertNotNull(result.getTo());
    }
}
