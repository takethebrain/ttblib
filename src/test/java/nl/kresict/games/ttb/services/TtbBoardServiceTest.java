package nl.kresict.games.ttb.services;

import junit.framework.TestCase;
import nl.kresict.games.ttb.domain.TtbField;
import nl.kresict.games.ttb.domain.TtbGameBoard;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbBoardServiceTest extends TestCase {
    
    public TtbBoardServiceTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of initBoard method, of class TtbBoardService.
     */
    public void testInitBoard() {
        TtbGameBoard result = TtbBoardService.initBoard();
        TtbField field1 = result.getField(0, 0);
        assertNotNull(field1);
    }
}
