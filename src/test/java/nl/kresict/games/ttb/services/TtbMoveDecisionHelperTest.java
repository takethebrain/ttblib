package nl.kresict.games.ttb.services;

import junit.framework.TestCase;
import nl.kresict.games.ttb.domain.TtbField;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.services.ex.EndOfGameException;
import nl.kresict.games.ttb.services.ex.HitException;
import nl.kresict.games.ttb.services.ex.IllegalMoveException;
import nl.kresict.games.ttb.services.ex.NinnyReplaceException;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbMoveDecisionHelperTest extends TestCase {
    
    public TtbMoveDecisionHelperTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

     public void testIsValidMoveOnlyOneDirection() throws EndOfGameException, HitException, NinnyReplaceException {
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 1;
        int fromY = 3;
        int toX = 3;
        int toY = 4;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        try {
            boolean result = TtbMoveDecisionHelper.isValidMove(board, from, to);
            assertEquals(result, false);
        } catch (IllegalMoveException ex) {
            assertEquals("Not your turn and not in one direction!", ex.getMessage());
        }
        
    }
    
    public void testIsValidMoveNotYourTurn() throws EndOfGameException, HitException, NinnyReplaceException {
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 1;
        int fromY = 3;
        int toX = 1;
        int toY = 4;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        try {
            TtbMoveDecisionHelper.isValidMove(board, from, to);
            assertTrue(false);
        } catch (IllegalMoveException ex) {
            assertEquals("Not your turn!", ex.getMessage());
        }
    }
    
    public void testIsValidMoveBrainNotMoreThenOne() throws EndOfGameException, HitException, NinnyReplaceException {
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 0;
        int fromY = 3;
        int toX = 0;
        int toY = 5;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        try {
            TtbMoveDecisionHelper.isValidMove(board, from, to);
            assertTrue(false);
        } catch (IllegalMoveException ex) {
            assertEquals("Not your turn!", ex.getMessage());
        }
    }
    
    public void testIsValidMoveNinnyNotMoreThenOne() throws EndOfGameException, HitException, NinnyReplaceException {
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 1;
        int fromY = 0;
        int toX = 3;
        int toY = 0;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        try {
            TtbMoveDecisionHelper.isValidMove(board, from, to);
            assertTrue(false);
        } catch (IllegalMoveException ex) {
            assertEquals("Not your turn!", ex.getMessage());
        }
    }
    
    public void testIsValidMoveNumskullMoreThenOne() {
        TtbGameBoard board = TtbBoardService.initBoard();

        TtbField from = board.getField(6, 5);
        TtbField to = board.getField(7, 6);
       
        TtbField from2 = board.getField(1, 0);
        TtbField to2 = board.getField(2, 0);
        
        TtbField from3 = board.getField(7, 5);
        TtbField to3 = board.getField(5, 5);
        try {
            boolean result;
            result = TtbMoveDecisionHelper.isValidMove(board, from, to);
            assertTrue(result);
            if (result) {
                board.movePiece(from, to);
            }
            result = TtbMoveDecisionHelper.isValidMove(board, from2, to2);
            assertTrue(result);
            if (result) {
                board.movePiece(from2, to2);
            }
            result = TtbMoveDecisionHelper.isValidMove(board, from3, to3);
            assertTrue(result);
            if (result) {
                board.movePiece(from3, to3);
            }
        } catch (IllegalMoveException ex) {
            System.out.println(ex.getMessage());
            assertTrue(false);
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
            assertTrue(false);
        }
    }
    
    public void testIsValidMoveCannotHitOwnPiece() throws EndOfGameException, HitException, NinnyReplaceException {
        System.out.println("testIsValidMoveCannotHitOwnPiece");
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 6;
        int fromY = 3;
        int toX = 6;
        int toY = 4;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        try {
            TtbMoveDecisionHelper.isValidMove(board, from, to);
            assertTrue(false);
        } catch (IllegalMoveException ex) {
            System.out.println(ex.getMessage());
            assertEquals("Can not hit your own piece!", ex.getMessage());
        }
    }
    
    public void testIsValidMoveCannotGoOverPiece() throws EndOfGameException, HitException, NinnyReplaceException {
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 7;
        int fromY = 2;
        int toX = 5;
        int toY = 2;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        try {
            TtbMoveDecisionHelper.isValidMove(board, from, to);
            assertTrue(false);
        } catch (IllegalMoveException ex) {
            assertEquals("Can not go over a piece!", ex.getMessage());
        }
    }
    
    public void testIsValidMoveCannotGoThatWay() throws EndOfGameException, HitException, NinnyReplaceException {
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 6;
        int fromY = 3;
        int toX = 5;
        int toY = 4;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        try {
            TtbMoveDecisionHelper.isValidMove(board, from, to);
            assertTrue(false);
        } catch (IllegalMoveException ex) {
            assertEquals("Can not go this way from field: {7,4}", ex.getMessage());
        }
    }
}
