package nl.kresict.games.ttb.services;

import java.util.List;
import junit.framework.TestCase;
import nl.kresict.games.ttb.domain.Move;
import nl.kresict.games.ttb.domain.TtbField;
import nl.kresict.games.ttb.domain.TtbGameBoard;
import nl.kresict.games.ttb.services.ex.IllegalMoveException;

/**
 *
 * @author Sander IJpma | kres-ict
 */
public class TtbMoveHelperTest extends TestCase {
    
    public TtbMoveHelperTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getMoves method, of class TtbMoveHelper.
     */
    public void testGetMovesToWest() throws IllegalMoveException {
        System.out.println("testGetMovesToWest");
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromY = 3;
        int fromX = 0;
        int toY = 1;
        int toX = 0;
        TtbField from = board.getField(fromX, fromY);
        
        TtbField to = board.getField(toX, toY);
        TtbMoveHelper instance = new TtbMoveHelper();
        List<Move> result = instance.getMoves(board, from, to);
        Move actualFrom = result.get(0);
        Move actualTo = result.get(result.size()-1);
        assertEquals(from, actualFrom.getFrom());
        assertEquals(to, actualTo.getTo());
        assertEquals(2, result.size());
        for (Move move : result) {
            String fromStr = move.getFrom().toStringCoordinates();
            String toStr = move.getTo().toStringCoordinates();
            System.out.println(fromStr + "->" + toStr);
        }
    }
    
    public void testGetMovesToEast() throws IllegalMoveException {
        System.out.println("testGetMovesToEast");
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromY = 1;
        int fromX = 0;
        int toY = 3;
        int toX = 0;
        TtbField from = board.getField(fromX, fromY);
        
        TtbField to = board.getField(toX, toY);
        TtbMoveHelper instance = new TtbMoveHelper();
        List<Move> result = instance.getMoves(board, from, to);
        Move actualFrom = result.get(0);
        Move actualTo = result.get(result.size()-1);
        assertEquals(from, actualFrom.getFrom());
        assertEquals(to, actualTo.getTo());
        assertEquals(2, result.size());
        for (Move move : result) {
            String fromStr = move.getFrom().toStringCoordinates();
            String toStr = move.getTo().toStringCoordinates();
            System.out.println(fromStr + "->" + toStr);
        }
    }
    
    public void testGetMovesToSouth() throws IllegalMoveException {
        System.out.println("testGetMovesToSouth");
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromY = 0;
        int fromX = 1;
        int toY = 0;
        int toX = 3;
        TtbField from = board.getField(fromX, fromY);
        
        TtbField to = board.getField(toX, toY);
        TtbMoveHelper instance = new TtbMoveHelper();
        List<Move> result = instance.getMoves(board, from, to);
        Move actualFrom = result.get(0);
        Move actualTo = result.get(result.size()-1);
        assertEquals(from, actualFrom.getFrom());
        assertEquals(to, actualTo.getTo());
        assertEquals(2, result.size());
        for (Move move : result) {
            String fromStr = move.getFrom().toStringCoordinates();
            String toStr = move.getTo().toStringCoordinates();
            System.out.println(fromStr + "->" + toStr);
        }
    }
    
    public void testGetMovesToNorth() throws IllegalMoveException {
        System.out.println("testGetMovesToNorth");
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 3;
        int fromY = 0;
        int toX = 1;
        int toY = 0;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        TtbMoveHelper instance = new TtbMoveHelper();
        List<Move> result = instance.getMoves(board, from, to);
        Move actualFrom = result.get(0);
        Move actualTo = result.get(result.size()-1);
        assertEquals(from, actualFrom.getFrom());
        assertEquals(to, actualTo.getTo());
        assertEquals(2, result.size());
        for (Move move : result) {
            String fromStr = move.getFrom().toStringCoordinates();
            String toStr = move.getTo().toStringCoordinates();
            System.out.println(fromStr + "->" + toStr);
        }
    }
    
    public void testGetMovesToSouthWest() throws IllegalMoveException {
        System.out.println("testGetMovesToSouthWest");
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 0;
        int fromY = 3;
        int toX = 3;
        int toY = 0;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        TtbMoveHelper instance = new TtbMoveHelper();
        List<Move> result = instance.getMoves(board, from, to);
        Move actualFrom = result.get(0);
        Move actualTo = result.get(result.size()-1);
        assertEquals(from, actualFrom.getFrom());
        assertEquals(to, actualTo.getTo());
        assertEquals(3, result.size());
        for (Move move : result) {
            String fromStr = move.getFrom().toStringCoordinates();
            String toStr = move.getTo().toStringCoordinates();
            System.out.println(fromStr + "->" + toStr);
        }
    }
    
    public void testGetMovesToSouthEast() throws IllegalMoveException {
        System.out.println("testGetMovesToSouthEast");
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 0;
        int fromY = 0;
        int toX = 3;
        int toY = 3;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        TtbMoveHelper instance = new TtbMoveHelper();
        List<Move> result = instance.getMoves(board, from, to);
        Move actualFrom = result.get(0);
        Move actualTo = result.get(result.size()-1);
        assertEquals(from, actualFrom.getFrom());
        assertEquals(to, actualTo.getTo());
        assertEquals(3, result.size());
        for (Move move : result) {
            String fromStr = move.getFrom().toStringCoordinates();
            String toStr = move.getTo().toStringCoordinates();
            System.out.println(fromStr + "->" + toStr);
        }
    }
    
    public void testGetMovesToNorthWest() throws IllegalMoveException {
        System.out.println("testGetMovesToNorthWest");
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 3;
        int fromY = 3;
        int toX = 0;
        int toY = 0;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        TtbMoveHelper instance = new TtbMoveHelper();
        List<Move> result = instance.getMoves(board, from, to);
        assertEquals(3, result.size());
        Move actualFrom = result.get(0);
        Move actualTo = result.get(result.size()-1);
        assertEquals(from, actualFrom.getFrom());
        assertEquals(to, actualTo.getTo());
        for (Move move : result) {
            String fromStr = move.getFrom().toStringCoordinates();
            String toStr = move.getTo().toStringCoordinates();
            System.out.println(fromStr + "->" + toStr);
        }
    }
    
    public void testGetMovesToNorthEast() throws IllegalMoveException {
        System.out.println("testGetMovesToNorthEast");
        TtbGameBoard board = TtbBoardService.initBoard();
        int fromX = 3;
        int fromY = 0;
        int toX = 0;
        int toY = 3;
        TtbField from = board.getField(fromX, fromY);
        TtbField to = board.getField(toX, toY);
        TtbMoveHelper instance = new TtbMoveHelper();
        List<Move> result = instance.getMoves(board, from, to);
        Move actualFrom = result.get(0);
        Move actualTo = result.get(result.size()-1);
        assertEquals(from, actualFrom.getFrom());
        assertEquals(to, actualTo.getTo());
        assertEquals(3, result.size());
        for (Move move : result) {
            String fromStr = move.getFrom().toStringCoordinates();
            String toStr = move.getTo().toStringCoordinates();
            System.out.println(fromStr + "->" + toStr);
        }
    }
    
}
